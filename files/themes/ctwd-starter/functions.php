<?php
// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php'); 

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php'); 

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php'); 

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php'); 
require_once(get_template_directory().'/assets/functions/menu-walkers.php'); 

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php'); 

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php'); 

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php'); 

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php'); 

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php'); 

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php'); 

// Use this as a template for custom post types
// require_once(get_template_directory().'/assets/functions/custom-post-type.php');

// Added and Included Aqua Resizer use -- aq_resize($img_url,$width);
 require_once(get_template_directory().'/assets/functions/aq_resizer.php');


// Added and Included Aqua Resizer use -- aq_resize($img_url,$width);
 //require_once(get_template_directory().'/assets/functions/ctwd-functions.php');

// Customize the WordPress login menu
 require_once(get_template_directory().'/assets/functions/login.php');

// Customize the WordPress admin
 require_once(get_template_directory().'/assets/functions/admin.php');

//See documentation here: https://github.com/ResponsiveImagesCG/wp-tevko-responsive-images
function custom_theme_setup() {
    add_theme_support( 'advanced-image-compression' );
}
add_action( 'after_setup_theme', 'custom_theme_setup' );


// img unautop
function img_unautop($pee) {
    $pee = preg_replace('/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '<div class="figure">$1</div>', $pee);
    return $pee;
}
add_filter( 'the_content', 'img_unautop', 30 );


// Generate image that is resized using Aqua Resize and that is output using Picturefill http://scottjehl.github.io/picturefill/
function ctwd_image($id,$small,$medium,$large) {
	$image_alt = get_post_meta($id, '_wp_attachment_image_alt', true);
	$image_url = wp_get_attachment_url( $id );
	$image_small = aq_resize( $image_url, $small['width'], $small['height'], true, true, true);
	if ($image_small == null) { $image_small = $image_url; }
	$image_medium = aq_resize( $image_url, $medium['width'], $medium['height'], true, true, true);
	if ($image_medium == null) { $image_medium = $image_url; }
	$image_large = aq_resize( $image_url, $large['width'], $large['height'], true, true, true);
	if ($image_large == null) { $image_large = $image_url; }
	$image = '<picture itemprop="image">
		<!--[if IE 9]><video style="display: none;"><![endif]-->
		<source srcset="' . $image_large .'" media="(min-width: 64.063em)">
		<source srcset="' . $image_medium . '" media="(min-width: 40.063em)">
		<!--[if IE 9]></video><![endif]-->

		<!--[if lt IE 9]>
		<img src="' . $image_large .'" alt="' . $image_alt . '">
		<![endif]-->

		<!--[if !lt IE 9]><!-->
		<img srcset="' . $image_small .'" alt="' . $image_alt . '">
		<![endif]-->
	</picture>';
	return $image;
}
?>
