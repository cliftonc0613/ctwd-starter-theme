<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(slideup); ?>

			<div id="content">
                <div class="owlcarosel-container">
                        <div class="row collapse full-width">

                                <div class="large-12 columns slides">
                                   <div class="owl-carousel">
                                   <?php if( have_rows('slides') ):
                                       while ( have_rows('slides') ) : the_row(); ?>
                                           <div class="item">
                                            <?php
                                            // This code uses Aqua Resize and Picturefill to dynamically size and load a photo uploaded and cropped by Advanced Custom Fields for multiple devices.
                                            $image_id = get_sub_field('slide');
                                            $small = array("width" => 640,"height" => 300);
                                            $medium = array("width" => 1025,"height" => 480);
                                            $large = array("width" => 2000,"height" => 600);
                                            echo ctwd_image($image_id,$small,$medium,$large);
                                            ?>
                                              <div class="slider-overlay">

                                                  <h2><?php echo get_sub_field('header'); ?></h2>
                                                  <p><?php echo get_sub_field('paragraph'); ?></p>
                                                  <a href="<?php echo get_sub_field('link_to'); ?>" class="slide-btn">Learn More</a>

                                              </div>
                                           </div>
                                       <?php endwhile;
                                   endif;
                                   ?>
                                   </div> <!-- bxslider -->
                                </div> <!-- slides -->
                             <!-- live-edit -->
                        </div>
                    </div> <!-- bxcontainer -->

				<div id="inner-content" class="row">
                
				    <div id="main" class="large-12 medium-12 columns" role="main">
                            
                            <div class="multiple-items">
                              <div>1</div>
                              <div>2</div>
                              <div>3</div>
                              <div>3</div>
                              <div>5</div>
                              <div>6</div>
                            </div>
						<?php get_template_part( 'parts/loop', 'page' ); ?>

    				</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->


<?php get_footer(); ?>

<script>

$('.owl-carousel').owlCarousel({
    loop:true,
    //margin:10,
    //nav:true,
    lazyLoad: true,
    autoplay: true,
    animateIn:'bounceInRight',
    animateOut: 'bounceOutLeft',
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

$('.multiple-items').slick({
   centerMode: true,
   autoplay: true,
  autoplaySpeed: 2000,
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3
});
</script>
