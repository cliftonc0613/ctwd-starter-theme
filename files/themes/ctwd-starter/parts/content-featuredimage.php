<!-- This outputs the featured image. This also works for the blog page.
Uses Aqua Resize and Picturefill to dynamically size and load the featured image for multiple devices.
library/drum-functions.php
-->
<?php if ( has_post_thumbnail() ) : ?>
	<div class="featured-image">
				<?php
				/** If this is the blog page then get the featured image for it **/
				if (is_home() && get_option('page_for_posts') ) {
					$image_id = get_post_thumbnail_id(get_option('page_for_posts'));
				} else {
					$image_id = get_post_thumbnail_id();
				}
				$small = array("width" => 640,"height" => 300);
				$medium = array("width" => 1025,"height" => 380);
				$large = array("width" => 2000,"height" => 350);
				echo ctwd_image($image_id,$small,$medium,$large);
				?>
               <div class="row">
                   <div class="large-6 medium-6 columns">
                       <header class="article-header">
                            <h1 class="page-title"><?php the_title(); ?></h1>
                       </header> <!-- end article header -->
                   </div>
               </div>

	</div> <!-- featured-image -->
<?php endif; ?>
