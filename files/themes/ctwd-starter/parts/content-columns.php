
<?php
/**
 *
 * The template for displaying column contents
 *
 */
?>

<div <?php if(function_exists("live_edit")){ live_edit('content'); }?>>
<?php
// check if the flexible content field has rows of data
if( have_rows('content') ):
     // loop through the rows of data
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'one_column' ): ?>
        	<div class="large-12 columns">
        		<?php echo apply_filters('the_content', get_sub_field('one_column')); ?>
			</div>
        <?php elseif( get_row_layout() == 'two_columns' ): ?>
        	<div class="large-6 medium-6 columns">
        		<?php echo apply_filters('the_content', get_sub_field('column_1')); ?>
        	</div>
        	<div class="large-6 medium-6 columns">
        		<?php echo apply_filters('the_content', get_sub_field('column_2')); ?>
        	</div>
        <?php elseif( get_row_layout() == 'three_columns' ): ?>
        	<div class="large-4 medium-4 columns">
        		<?php echo apply_filters('the_content', get_sub_field('column_1')); ?>
        	</div>
        	<div class="large-4 medium-4 columns">
        		<?php echo apply_filters('the_content', get_sub_field('column_2')); ?>
        	</div>
        	<div class="large-4 medium-4 columns">
        		<?php echo apply_filters('the_content', get_sub_field('column_3')); ?>
        	</div>
        <?php elseif( get_row_layout() == 'four_columns' ): ?>
        	<div class="large-3 medium-3 columns">
        		<?php echo apply_filters('the_content', get_sub_field('column_1')); ?>
        	</div>
        	<div class="large-3 medium-3 columns">
        		<?php echo apply_filters('the_content', get_sub_field('column_2')); ?>
        	</div>
        	<div class="large-3 medium-3 columns">
        		<?php echo apply_filters('the_content', get_sub_field('column_3')); ?>
        	</div>
        	<div class="large-3 medium-3 columns">
        		<?php echo apply_filters('the_content', get_sub_field('column_4')); ?>
        	</div>
        <?php elseif( get_row_layout() == 'right_sidebar' ): ?>
        	<div class="large-8 medium-8 columns">
        		<?php echo apply_filters('the_content', get_sub_field('wide_column')); ?>
        	</div>
        	<div class="large-4 medium-4 columns">
        		<?php echo apply_filters('the_content', get_sub_field('narrow_column')); ?>
        	</div>
        <?php elseif( get_row_layout() == 'left_sidebar' ): ?>
        	<div class="large-4 medium-4 columns">
        		<?php echo apply_filters('the_content', get_sub_field('narrow_column')); ?>
        	</div>
        	<div class="large-8 medium-8 columns">
        		<?php echo apply_filters('the_content', get_sub_field('wide_column')); ?>
        	</div>
        <?php endif;
    endwhile;
endif;
?>
</div> <!-- live edit -->
