CTWD Starter Theme
Demo: www.ctwdstarter.ctwdsdemos.com

### What is CTWD Starter Theme?
CTWD Starter Theme is a blank WordPress theme built with Foundation 5, giving you all the power and flexibility you need to build complex, mobile friendly websites without having to start from scratch.

Starting its humble life as a fork of the popular theme Bones, CTWD Starter Theme is now the foundation of thousands of websites across the globe.

### What comes with CTWD Starter Theme?
CTWD Starter Theme comes pre-baked with all of the great features that are found in the Foundation framework – simply put, if it works in Foundation, it will work in CTWD Starter Theme. The theme also includes:

- Foundation Navigation Options
- Grid and Accordion archive templates
- Translation Support
- Bower and Gulp Support
- Advance Custom Feilds Customization
- Owl Carousel 2
- Slick Slider

### What tools do I need to use CTWD Starter Theme?
You can use whatever you want – seriously. While the Sass version comes with Bower and Gulp support out of the box, you aren’t required to use those by any means. You can use CodeKit, Grunt, Compass or nothing at all. It’s completely up to you how you decide to build you theme – CTWD Starter Theme won’t get in the way of your workflow.