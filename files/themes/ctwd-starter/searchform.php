<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<div class="row collapse postfix-round">
		<div class="small-8 columns">
			<label>
				<span class="screen-reader-text hide"><?php echo _x( 'Search for:', 'label', 'jointstheme' ) ?></span>
				<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search...', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'jointstheme' ) ?>" />
			</label>
		</div>
	  <div class="small-4 columns">
				<input type="submit" class="search-submit button postfix" value="<?php echo esc_attr_x( 'Search', 'jointstheme' ) ?>" />
	  </div>
	</div>
</form>
