<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'ctwdsdem_ctwdstarter');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'ctwdsdem_ctwdusr');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'zmK*p5XR_?+#');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rbiTPwv*&[GMv{=3ow2us-v*0|Tg((o<|-7xbR`?9Cri>+nOWq8=}!/I{<g+BDc=');
define('SECURE_AUTH_KEY',  '$B_7,-*N|2y-[4?1<2kzR|Qj +[Aw~a0mfJSSn-(C/2[!>h?Y.OZO#wrw9YKp-*#');
define('LOGGED_IN_KEY',    '0-q<=*:3Cs8?u+q1+Hr)=8;g!]7!eE@6*Nc1[`+#&*6] ZnqD{b:4(&lom=mmu+5');
define('NONCE_KEY',        'ku/9>z;kCr;M@}+zl|]mub-[9D8>.++cv^!2(?yU;IWGI);9Bof|M?bhw]D}e9?d');
define('AUTH_SALT',        '_}Fs?8&+Iph/D OG}(awOGTkVf<9.$4@JvG`>CK+Hl)CM,:CeM!BLe56uD!DR3Jo');
define('SECURE_AUTH_SALT', 'F+N9|`.SW01NpW|z0f}F@swM+zH<Tm,m(Hp[Zc59|-}fx=,-r|v:de@VZSc+WGdj');
define('LOGGED_IN_SALT',   '~fn$`;|Gcswhu~XvpLc}-DptB%n=>r+U|m0r-4@` `TFC*5(HCG]<qZ(7p_Xo{i5');
define('NONCE_SALT',       'YJq|I[3*+J]&<J2&c+vA:YZ!|-`kEu*FZBJ/Ev4 ZLJ!2+lV| ps** W!BMb$/Al');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ctwd_starter_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');


/**
 * Set custom paths
 *
 * These are required because wordpress is installed in a subdirectory.
 */
if (!defined('WP_SITEURL')) {
	define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp');
}
if (!defined('WP_HOME')) {
	define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME'] . '');
}
if (!defined('WP_CONTENT_DIR')) {
	define('WP_CONTENT_DIR', dirname(__FILE__) . '/files');
}
if (!defined('WP_CONTENT_URL')) {
	define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/files');
}


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
